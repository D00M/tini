#ifndef TINI_H_
#define TINI_H_

#include <stdbool.h>
#include <stdio.h>
#include <tinyutil/vector.h>

struct tini_pair {
    char *key, *value;
};

struct tini_section {
    // If the section name is NULL then it is the "global" section
    char *name; // can be NULL
    struct utl_vector key_values; // struct tini_key_value
};

struct tini {
    struct tini_section *current;
    struct utl_vector sections; // struct tini_section
};

bool tini_init(struct tini *ini);
bool tini_destroy(struct tini *ini);
bool tini_parse(struct tini *ini, FILE *file);

#endif // TINI_H_
