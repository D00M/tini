#include "tini.h"
#include <assert.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>


int main(int argc, char **argv) {
    assert(argc > 1);
    int32_t code = EXIT_SUCCESS;
    struct tini tini;
    tini_init(&tini);

    if (!tini_parse(&tini, argv[1])) code = EXIT_FAILURE;

    struct tini_section *section;
    utl_vector_for_each(section, &tini.sections) {
        fprintf(stderr, "[%s]\n", section->name ? section->name : "global");
        struct tini_pair *pair;
        utl_vector_for_each(pair, &section->key_values) {
            fprintf(stderr, "%s = %s\n", pair->key, pair->value);
        }
        fprintf(stderr, "\n");
    }

    tini_destroy(&tini);

    return code;
}
