PREFIX 	 := /usr
PKG_CONFIG_DIR := ${PREFIX}/lib/pkgconfig
INCLUDE  := ./include
SRC 	 := ./src

INCLUDES := ${INCLUDE}/tini.h
SRCS	 := ${SRC}/tini.c
OBJS	 := ${patsubst %.c,%.o,${SRCS}}

EXAMPLED   := ./example
EXAMPLESRC := ${EXAMPLED}/main.o

all: static
static: tini.a
dynamic: tini.so
object: tini.o
example: ${EXAMPLED}/example

tini.o: ${SRCS}

tini.a: clean ${OBJS} tinyutil.a
	ar -x tinyutil.a
	ar -rc $@ *.o

tini.so: LIBS += -ltinyutil
tini.so: clean ${OBJS}
	${CC} ${OBJS} -fPIC -shared -o $@ ${CFLAGS} ${LIBS}

install:
	install -Dm 644 ${INCLUDE}/tini.h ${PREFIX}/include/tini.h

installdynamic: install dynamic
	install -Dm 644 tini.so ${PREFIX}/lib/tini.so
	install -Dm 644 tini.pc ${PKG_CONFIG_DIR}/tini.pc

${EXAMPLED}/example: tini.a ${EXAMPLESRC}
	${CC} ${EXAMPLESRC} tini.a -o $@ ${CFLAGS} ${LIBS}
	./${EXAMPLED}/example ./example/test.ini

%.o: CFLAGS += -I./tinyutil/include/ -I${INCLUDE}
%.o: %.c
	${CC} -c $< -o $@ ${CFLAGS} ${LIBS}

tinyutil.a:
	cd tinyutil; ${MAKE}; mv tinyutil.a ../

clean:
	rm -rf *.so *a *.o ${EXAMPLED}/*.o ${EXAMPLED}/example
