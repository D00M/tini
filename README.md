# Tini
The tiny ini parser.

# Dependencies
 + [tinyutil](https://codeberg.org/D00M/tinyutil). Tinyutil is included as a submodule in this repo, but see [Compile](#compile) for specifics on where this is used.

# Compile
There are multiple targets for compilation:
 + `object` for an object file which excludes tinyutil object files.
 + `static` for an archive file including tinyutil object files.
 + `dynamic` for an shared object file this requires tinyutil to be installed as a shared object library.

# Install
The `install` target installs `tini.h`. `installdyanmic` installs `tini.h` and the shared object file along with the pkg-config file.

# TODOs
+ [ ] Add the ability to set a logger for the user.
