#include "tini.h"
#include <ctype.h>
#include <errno.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <tinyutil/vector.h>

#define length(arr) (sizeof(arr) / sizeof(arr[0]))
#define string_equal(str1, str2) (strcmp(str1, str2) == 0)

bool tini_init(struct tini *ini) {
    if (!ini) {
        errno = EINVAL;
        return false;
    }

    ini->current = NULL;
    utl_vector_init(&ini->sections, sizeof(struct tini_section));

    return true;
}

bool tini_destroy(struct tini *ini) {
    if (!ini) {
        errno = EINVAL;
        return false;
    }

    struct tini_section *section;
    utl_vector_for_each(section, &ini->sections) {
        struct tini_pair *pair;
        utl_vector_for_each(pair, &section->key_values) {
            free(pair->key);
            free(pair->value);
        }

        free(section->name);
        utl_vector_destroy(&section->key_values);
    }

    utl_vector_destroy(&ini->sections);

    return true;
}

static bool get_line(FILE *file, char **line, size_t *len) {
    if (!file || !len || !line) return false;

    if (*line || *len) {
        **line = '\0';
        *len = 0;
    }

    int ch = fgetc(file);
    char buf[20];
    size_t i = 0,
           k = i;

    while (ch != '\n' && ch != EOF) {
        // ignore leading spaces to make things easy.
        if (k == 0 && i == 0 && isspace(ch)) goto next;

        buf[i++] = ch;

        if (i == length(buf)) {
            *line = realloc(*line, sizeof(char) * (k + i + 1));
            memcpy(*line + k, buf, sizeof(char) * i);
            k += i;
            i = 0;
        }

next:
        ch = fgetc(file);
    }

    if (i != 0) {
        *line = realloc(*line, sizeof(char) * (k + i + 1));
        memcpy(*line + k, buf, sizeof(char) * i);
        *len = k + i - 1;

        // Remove spaces at the end
        for (; *len >= 0 && isspace((*line)[*len]); (*len)--);
        (*line)[*len + 1] = '\0';
    }

    return ch != EOF;
}

static bool tini_section_init(struct tini_section *section, char *name) {
    if (!section) return false;

    section->name = name;
    utl_vector_init(&section->key_values, sizeof(struct tini_pair));

    return true;
}

static bool tini_section(struct tini *ini, char *line, size_t len) {
    if (!ini || !line || !len) return false;

    for (;*line && (*line == '[' || isspace(*line)); line++, len--);
    for (; len >= 0 && (line[len] == ']' || isspace(line[len])); len--);
    line[len + 1] = '\0';

    char *name = line;
    struct tini_section *section = utl_vector_add(&ini->sections);
    if (!section) return false;

    tini_section_init(section, strdup(name));

    ini->current = section;

    return true;
}

static bool tini_pair(struct tini *ini, char *line, size_t len) {
    if (!ini || !line || !len) return false;

    if (!ini->current) {
        ini->current = utl_vector_add(&ini->sections);
        if (!ini->current) return false;
        tini_section_init(ini->current, NULL);
    }

    char *right = line;
    char *left = strsep(&right, "=");

    size_t i;
    for (i = strlen(left) - 1; i >= 0 && isspace(left[i]); i--);
    left[i + 1] = '\0';

    for (; *right && isspace(*right); right++);

    if (!strlen(left)) {
        fprintf(stderr, "key name is empty");
        return false;
    }

    utl_vector_push(&ini->current->key_values, &(const struct tini_pair){
            .key = strdup(left),
            .value = strdup(right),
            });

    return true;
}

bool tini_parse(struct tini *ini, FILE *file) {
    if (!ini || !file) {
        errno = EINVAL;
        return false;
    }

    char *line = NULL;
    size_t len = 0;

    bool out = true;
    while (out) {
        if (!get_line(file, &line, &len)) break;

        if (line[0] == '#' || len == 0) {
            continue;
        }
        else if (line[0] == '[' ) {
            if (line[len] != ']') {
                fprintf(stderr, "expected a section but got something else: '%s'\n", line);
                continue;
            }

            if (!tini_section(ini, line, len)) out = false;
        }
        else if (strchr(line, '=')) {
            if (!tini_pair(ini, line, len)) out = false;
        }
        else {
            fprintf(stderr, "expected a pair but got something else: '%s'\n", line);
            continue;
        }
    }

    free(line);

    return out;
}
